package edu.rit.cs.basic_word_count;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.InetAddress;

//this is the client class for subtask 3

public class WordCount_Worker {
    public static void main(String args[]) throws Exception{
        Socket s = null;
        try{
            int serverPort = 1234;

            //It uses LocalHost so it is hard coded to run on the same machine
            s = new Socket(InetAddress.getLocalHost(), serverPort);
            ObjectInputStream in = new ObjectInputStream( s.getInputStream());
            ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());

            //here the data is read from the server
            Object data = in.readObject();
            List<AmazonFineFoodReview> Allreviews = (List<AmazonFineFoodReview>)data;

            //preforms operations to get the treemap with all the words and their counts
            List<String> words = new ArrayList<String>();
            for(AmazonFineFoodReview review : Allreviews) {
                Pattern pattern = Pattern.compile("([a-zA-Z]+)");
                Matcher matcher = pattern.matcher(review.get_Summary());

                while(matcher.find())
                    words.add(matcher.group().toLowerCase());
            }

            TreeMap<String, Integer> wordcount = new TreeMap<>();
            for(String word : words) {
                if(!wordcount.containsKey(word)) {
                    wordcount.put(word, 1);
                } else{
                    int init_value = wordcount.get(word);
                    wordcount.replace(word, init_value, init_value+1);
                }
            }

            //here the data is written back to the server as an object
            out.writeObject(wordcount);
        }catch (UnknownHostException e){
            System.out.println("Sock:"+e.getMessage());
        }catch (EOFException e){System.out.println("EOF:"+e.getMessage());
        }catch (IOException e){System.out.println("IO:"+e.getMessage());
        }finally {if(s!=null) try {s.close();}catch (IOException e){System.out.println("close:"+e.getMessage());}}
    }
}
