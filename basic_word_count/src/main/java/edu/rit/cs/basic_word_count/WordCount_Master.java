package edu.rit.cs.basic_word_count;


import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

//this is the TCP server class for subtask 3, I used the same design as the one that you put on the slides
//my code is hard coded to use one server and 5 clients so it wont finish until it's formed 5 connections
//I did not time this subtask because the majority of the time would come from me getting all the clients running
//and waiting for the server to be ready to accept clients

public class WordCount_Master {

    public static TreeMap<String,Integer> result = new TreeMap<>();
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";
    public static int count = 0;

    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    public static void print_word_count( TreeMap<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static void main(String args[]) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        //this program is hard coded to use 1 server node and 5 client nodes
        //so here the data is partitioned into 5 groups
        int size = allReviews.size();
        int range = size/5;
        int extra = size%5;


        try {
            int serverPort = 1234;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            int i = 0;
            //here the data is partitioned and the connections are started
            System.out.println("Ready to connect");
            while(i < 5) {
                List partitionedList = new ArrayList<>();
                if(i==4){
                    partitionedList.addAll(allReviews.subList(range*i, range*(i+1)+extra));
                }
                else {
                    partitionedList.addAll(allReviews.subList(range * i, range * (i + 1)));
                }
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket,partitionedList);
                i++;
            }
        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
        //this loops waits for all worker nodes to merge their data before printing the final result
        while(true) {
            if(count==5) {
                break;
            }
        }
        print_word_count(result);
    }

    //this function merges the treemaps together and increments count to let the program know that another
    //worker node has finished
    public static void merge(TreeMap<String,Integer> map){

        map.forEach(
                (key, value) -> result.merge( key, value, (v1, v2) ->  v1 + v2)
        );
        count++;
    }
}


class Connection extends Thread {
    DataInputStream in;
    DataOutputStream out;
    Socket clientSocket;
    List partitionedData;
    ObjectOutputStream oos;
    ObjectInputStream ois;

    public Connection(Socket aClientSocket, List thepartitionedData) {
        try {
            clientSocket = aClientSocket;
            partitionedData = thepartitionedData;
            in = new DataInputStream(clientSocket.getInputStream());
            oos = new ObjectOutputStream(clientSocket.getOutputStream());
            ois = new ObjectInputStream(clientSocket.getInputStream());
            this.start();
        } catch (IOException e) {
            System.out.println("Connection:" + e.getMessage());
        }
    }

    public void run(){
        try {
            //writes the partitioned data list to the client
            oos.writeObject(partitionedData);
            //reads the data received from the client and casts it as a treemap
            Object data = ois.readObject();
            TreeMap<String,Integer> map = (TreeMap<String,Integer>)data;
            WordCount_Master.merge(map);

        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        }catch (ClassNotFoundException e) {
            System.out.println("Class:" + e.getMessage());
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {/*close failed*/}
        }
    }
}

