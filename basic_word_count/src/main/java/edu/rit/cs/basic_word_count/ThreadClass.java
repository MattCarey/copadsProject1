package edu.rit.cs.basic_word_count;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


// @author: Matthew Carey
// this class is for subtask 2. It takes in a partitioned list of reviews and preforms the operations on them
// to get the treemap of words with their counts

public class ThreadClass implements Callable {
    List<AmazonFineFoodReview> allReviews;

    public ThreadClass(List<AmazonFineFoodReview> list){
        allReviews = list;
    }

    @Override
    public Object call() throws Exception {
        List<String> words = new ArrayList<String>();
        for(AmazonFineFoodReview review : allReviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                words.add(matcher.group().toLowerCase());
        }

        TreeMap<String, Integer> wordcount = new TreeMap<>();
        for(String word : words) {
            if(!wordcount.containsKey(word)) {
                wordcount.put(word, 1);
            } else{
                int init_value = wordcount.get(word);
                wordcount.replace(word, init_value, init_value+1);
            }
        }
        return wordcount;
    }
}
