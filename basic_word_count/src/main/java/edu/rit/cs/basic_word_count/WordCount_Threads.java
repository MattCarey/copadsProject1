package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.concurrent.*;

//@author: Matthew Carey
//This is the main class for subtask 2
//generally subtask 1 ran in about 1.7 seconds on my computer and this runs at about 0.7 seconds

public class WordCount_Threads {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="amazon-fine-food-reviews/Reviews.csv";

    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    public static void print_word_count( TreeMap<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    public static void main(String[] args) throws Exception {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        /* For debug purpose */
//        for(AmazonFineFoodReview review : allReviews){
//            System.out.println(review.get_Text());
//        }
        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();

        //hard coded to use 4 threads so the data is partitioned into 4 sets
        int size = allReviews.size();
        int range = size/4;
        int extra = size%4;


        FutureTask[] threads = new FutureTask[4];

        //here the data is partitioned and the threads are started
        for (int i = 0; i < 4; i++){
            List partitionedList = new ArrayList<>();
            if(i==3){
                partitionedList.addAll(allReviews.subList(range*i, range*(i+1)+extra));
            }
            else {
                partitionedList.addAll(allReviews.subList(range * i, range * (i + 1)));
            }
            Callable callable = new ThreadClass(partitionedList);

            threads[i] = new FutureTask(callable);

            Thread t = new Thread(threads[i]);
            t.start();
        }

        TreeMap<String,Integer> wordcount = new TreeMap<>();

        //here the data from the threads is collected and merged into a single treemap
        for (int i = 0; i < 4; i++){
            TreeMap<String,Integer> map = (TreeMap<String,Integer>)threads[i].get();
            map.forEach(
                    (key, value) -> wordcount.merge( key, value, (v1, v2) ->  v1 + v2)
            );
        }

        myTimer.stop_timer();
        print_word_count(wordcount);

        myTimer.print_elapsed_time();
    }

}

